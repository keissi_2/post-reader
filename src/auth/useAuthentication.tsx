import dayjs from 'dayjs';
import { useReducer } from 'react';

export interface AuthenticationState {
  user: {
    name: string;
    email: string;
  } | null;
  loggedIn: boolean;
  token: {
    value: string;
    received: Date;
  } | null;
}

const initialState: AuthenticationState = {
  user: null,
  loggedIn: false,
  token: null,
};

const clearUserDataFromLocalStorage = () => {
  localStorage.removeItem('user');
};

const setUserDataToLocalStorage = (userData: AuthenticationState) => {
  localStorage.setItem('user', JSON.stringify(userData));
};

const getUserDataFromLocalStorage = (): AuthenticationState | null => {
  const data = localStorage.getItem('user');

  if (data) {
    return JSON.parse(data) as AuthenticationState;
  }

  return null;
};

const isTokenValid = (tokenReceived: Date): boolean => {
  const now = dayjs();
  const tokenTime = dayjs(tokenReceived);

  return now.diff(tokenTime, 'hour') < 1;
};

const authenticationReducer = (
  state: AuthenticationState,
  action: { type: string; payload?: { [key: string]: any } }
) => {
  switch (action.type) {
    case 'log_in':
      const newState = {
        ...state,
        loggedIn: true,
        ...action.payload,
      };
      setUserDataToLocalStorage(newState);
      return newState;
    case 'log_out': {
      clearUserDataFromLocalStorage();
      return { ...initialState };
    }
    default:
      return { ...state };
  }
};

export const useAuthentication = (): [
  AuthenticationState,
  React.Dispatch<{ type: string; payload?: { [key: string]: any } }>,
  () => AuthenticationState | null,
  (tokenReceived: Date) => boolean
] => {
  const [user, dispatch] = useReducer(authenticationReducer, initialState);

  return [user, dispatch, getUserDataFromLocalStorage, isTokenValid];
};
