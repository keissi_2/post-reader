import React, {
  useState,
  useEffect,
  createContext,
  useContext,
  PropsWithChildren,
  ReactElement,
} from 'react';
import type { AuthenticationState } from './useAuthentication';
import { useAuthentication } from './useAuthentication';

const authenticationContext = createContext<{
  authenticationState: AuthenticationState;
  dispatch: React.Dispatch<{
    type: string;
    payload: { [key: string]: any };
  }> | null;
}>({
  authenticationState: {
    user: null,
    loggedIn: false,
    token: null,
  },
  dispatch: null,
});

export function AuthenticationProvider({
  children,
}: PropsWithChildren<any>): ReactElement {
  const [initialized, setInitialized] = useState(false);
  const [
    authenticationState,
    dispatch,
    getUserDataFromLocalStorage,
    isTokenValid,
  ] = useAuthentication();

  useEffect(() => {
    const userData = getUserDataFromLocalStorage();
    if (
      userData !== null &&
      userData.token &&
      isTokenValid(userData.token.received) &&
      !authenticationState.loggedIn
    ) {
      dispatch({ type: 'log_in', payload: userData });
    } else if (
      userData !== null &&
      userData.token &&
      !isTokenValid(userData.token.received)
    ) {
      dispatch({ type: 'log_out' });
    }
    setInitialized(true);
  }, [
    dispatch,
    isTokenValid,
    getUserDataFromLocalStorage,
    authenticationState.loggedIn,
  ]);

  return !initialized ? (
    <div />
  ) : (
    <authenticationContext.Provider value={{ authenticationState, dispatch }}>
      {children}
    </authenticationContext.Provider>
  );
}

export function useAuthenticationContext() {
  return useContext(authenticationContext);
}
