import { makeRequest } from '../utils';
import config from '../config';

interface RegisterRequestBody {
  client_id: string;
  email: string;
  name: string;
}

interface RegisterRequestResponse {
  meta: {
    request_id: string;
  };
  data: {
    client_id: 'ju16a6m81mhid5ue1z3v2g0uh';
    email: string;
    sl_token: string;
  };
}

export const register = async (name: string, email: string) => {
  const { clientId } = config;
  const requestBody: RegisterRequestBody = {
    client_id: clientId,
    name,
    email,
  };

  return await makeRequest<RegisterRequestResponse>(
    'POST',
    'https://api.supermetrics.com/assignment/register',
    undefined,
    requestBody
  );
};
