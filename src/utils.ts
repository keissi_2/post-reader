type HttpMethod = 'GET' | 'POST';

const HttpOk: 200 = 200;

export const makeRequest = async <T>(
  method: HttpMethod,
  url: string,
  queryParameters?: string[],
  body?: { [key: string]: any }
): Promise<[T, null] | [null, Error]> => {
  try {
    const response = await fetch(
      queryParameters ? `${`${url}?`.concat(queryParameters.join('&'))}` : url,
      {
        headers: {
          'Content-Type': 'application/json',
        },
        method,
        body: JSON.stringify(body),
      }
    );
    if (response.status !== HttpOk) {
      return [
        null,
        new Error(`Request failed with status code: ${response.status}`),
      ];
    }

    return [await response.json(), null];
  } catch (error) {
    return [null, error];
  }
};

export type SortOrder = 'Ascending' | 'Descending';

export const sortFnWithOrder =
  <T>(sortOrder: SortOrder, valueToCompare: (value: T) => any) =>
  (a: T, b: T): -1 | 0 | 1 => {
    if (valueToCompare(a) === valueToCompare(b)) {
      return 0;
    }

    if (sortOrder === 'Ascending') {
      return valueToCompare(a) > valueToCompare(b) ? 1 : -1;
    } else {
      return valueToCompare(a) > valueToCompare(b) ? -1 : 1;
    }
  };

export const sortArray = <T>(
  arr: T[],
  sortFn: (a: T, b: T) => 1 | 0 | -1
): T[] => {
  return [...arr.sort(sortFn)];
};
