import React from 'react';
import {
  Switch,
  BrowserRouter as Router,
  Route,
  Redirect,
  useRouteMatch,
} from 'react-router-dom';

import {
  AuthenticationProvider,
  useAuthenticationContext,
} from './auth/AuthenticationContext';
import { Login } from './pages/Login/Login';
import { Posts as PostsPage } from './pages/Posts/Posts';

function App() {
  return (
    <AuthenticationProvider>
      <Router>
        <Switch>
          <Route exact path="/login">
            <Login />
          </Route>
          <Route path={'/posts'}>
            <Posts />
          </Route>
          <Route exact path={'/'}>
            <Redirect to={'/posts'} />
          </Route>
          <Route path="*">404</Route>
        </Switch>
      </Router>
    </AuthenticationProvider>
  );
}

function Posts() {
  const { authenticationState } = useAuthenticationContext();
  const { path } = useRouteMatch();

  if (path === '/') {
    return (
      <Redirect
        to={{
          pathname: '/posts',
        }}
      />
    );
  }

  if (!authenticationState.loggedIn) {
    return (
      <Redirect
        to={{
          pathname: '/login',
        }}
      />
    );
  }

  return (
    <Switch>
      <Route exact path={path}>
        <PostsPage />
      </Route>
      <Route
        exact
        path={`${path}/:userId`}
        render={({ match }) => {
          const {
            params: { userId },
          } = match;

          return <PostsPage selectedSender={userId} />;
        }}
      />
      <Route path="*">404</Route>
    </Switch>
  );
}

export default App;
