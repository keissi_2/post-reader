import React, { FormEvent, useState } from 'react';
import { register } from '../../auth/register';
import { useHistory } from 'react-router';

import './login.css';
import { useAuthenticationContext } from '../../auth/AuthenticationContext';

interface LoginForm {
  name: string;
  email: string;
}

const handleSubmit =
  (
    form: LoginForm,
    dispatch: React.Dispatch<{ type: string; payload: { [key: string]: any } }>,
    goToPosts: () => void
  ) =>
  async (e: FormEvent) => {
    e.preventDefault();
    const { name, email } = form;

    if (!name || !email) {
      return;
    }

    const [data, error] = await register(name, email);

    if (data) {
      dispatch({
        type: 'log_in',
        payload: {
          user: { name, email },
          token: { value: data.data.sl_token, received: new Date() },
        },
      });
      goToPosts();
    } else {
      console.error(error);
    }
  };

const handleChange = (
  name: string,
  setForm: (form: LoginForm) => void,
  currentForm: LoginForm,
  value: string
) => {
  setForm({ ...currentForm, [name]: value });
};

export const Login = () => {
  const history = useHistory();
  const { dispatch } = useAuthenticationContext();
  const [form, setForm] = useState<LoginForm>({
    name: '',
    email: '',
  });

  const goToPosts = () => history.push('/posts');

  if (!dispatch) {
    return <div />;
  }

  return (
    <div className="login-form-container">
      <form id="login-form" onSubmit={handleSubmit(form, dispatch, goToPosts)}>
        <h1>Registration</h1>
        <div className="form-field">
          <label htmlFor="name">Name</label>
          <input
            onChange={(e) =>
              handleChange('name', setForm, form, e.currentTarget.value)
            }
            type="text"
            name="name"
            value={form.name}
          ></input>
        </div>
        <div className="form-field">
          <label htmlFor="email">Email</label>
          <input
            onChange={(e) =>
              handleChange('email', setForm, form, e.currentTarget.value)
            }
            type="email"
            name="email"
            value={form.email}
          ></input>
        </div>
        <div className="form-field">
          <button type="submit">Register</button>
        </div>
      </form>
    </div>
  );
};
