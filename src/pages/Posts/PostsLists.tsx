import React, { useMemo, useState } from 'react';
import dayjs from 'dayjs';
import { flow, curry } from 'lodash/fp';

import { Post } from './Post/Post';
import { sortArray, sortFnWithOrder } from '../../utils';
import { SearchInput } from '../../components/SearchInput/SearchInput';
import { LoadingIndicator } from '../../components/LoadingIndicator/LoadingIndicator';
import { SortBar } from '../../components/SortBar/SortBar';

import type { SortOrder } from '../../utils';
import type { PostsMap } from '../../posts/usePosts';

type PostType = PostsMap['posts'][number];

interface PostsProps {
  posts: PostType[];
  loadingPosts: boolean;
  selectedSender: string | null;
}

const filterPosts =
  (filterFn: (post: PostType) => boolean) => (posts: PostType[]) =>
    posts.filter(filterFn);

const sortPosts =
  (sortFn: (a: PostType, b: PostType) => -1 | 0 | 1) => (posts: PostType[]) =>
    sortArray<PostType>(posts, sortFn);

const sortPostsByCreationDate = (sortOrder: SortOrder) =>
  sortFnWithOrder<PostType>(sortOrder, (post) =>
    dayjs(post.created_time).unix()
  );

const isSelectedSender = curry(
  (selectedSender: string | null, post: PostType) =>
    !selectedSender ? true : selectedSender === post.from_id
);

const postContainsText = curry((text: string | null, post: PostType) =>
  text && text.length >= 3 ? post.message.indexOf(text) !== -1 : true
);

export const PostsList = ({
  posts,
  loadingPosts,
  selectedSender,
}: PostsProps) => {
  const [sortOrder, setSortOrder] = useState<SortOrder>('Ascending');
  const [containsText, setContainsText] = useState('');
  const sortAndFilterPosts = useMemo(
    () =>
      flow(
        filterPosts(isSelectedSender(selectedSender)),
        filterPosts(postContainsText(containsText)),
        sortPosts(sortPostsByCreationDate(sortOrder))
      ),
    [selectedSender, sortOrder, containsText]
  );

  return (
    <div id="posts-list">
      <div id="posts-list-controls">
        <SortBar sortOrder={sortOrder} changeSortOrder={setSortOrder} />
        <SearchInput
          label="Search messages:"
          name="search-posts"
          onChange={setContainsText}
          value={containsText}
          className="search-posts"
        />
      </div>
      {loadingPosts && (
        <div>
          <LoadingIndicator />
        </div>
      )}
      <div className="posts">
        {sortAndFilterPosts(posts).map((post) => (
          <Post
            key={post.id}
            message={post.message}
            created={post.created_time}
            sender={post.from_name}
          />
        ))}
      </div>
    </div>
  );
};
