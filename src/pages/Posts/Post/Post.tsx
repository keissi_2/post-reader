import React from 'react';
import dayjs from 'dayjs';

import './post.css';

interface PostProps {
  created: string;
  message: string;
  sender: string;
}

const format = 'MMMM D, YYYY HH:mm:ss';

export const Post = ({ sender, created, message }: PostProps) => {
  return (
    <div className="post">
      <div className="heading">
        <div className="timestamp">{dayjs(created).format(format)}</div>
        <div className="sender">{sender}</div>
      </div>
      <div className="message">{message}</div>
    </div>
  );
};
