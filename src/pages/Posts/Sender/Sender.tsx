import React from 'react';

import './sender.css';

interface SenderProps {
  name: string;
  postCount: number;
  isSelected: boolean;
}

export const Sender = ({ name, postCount, isSelected }: SenderProps) => {
  return (
    <div className={`sender ${isSelected ? 'selected' : ''}`}>
      <div className="name">{name}</div>
      <div className="post-count">
        <span className="badge">{postCount}</span>
      </div>
    </div>
  );
};
