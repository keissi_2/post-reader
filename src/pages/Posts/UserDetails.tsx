import React from 'react';

import './user-details.css';

interface UserDetailsProps {
  name: string;
  logOut: () => void;
}

export const UserDetails = ({ name, logOut }: UserDetailsProps) => {
  return (
    <div className="user-details">
      Logged in as <span className="name">{name} </span>
      <button className="logout-button" onClick={() => logOut()}>
        Log out
      </button>
    </div>
  );
};
