import React from 'react';
import { SearchInput } from '../../components/SearchInput/SearchInput';
import { SortBar } from '../../components/SortBar/SortBar';

import type { SortOrder } from '../../utils';

interface SenderListControlsProps {
  sortOrder: SortOrder;
  changeSortOrder: (sortOrder: SortOrder) => void;
  filterBy: string;
  setFilterBy: (value: string) => void;
}

export const SenderListControls = ({
  sortOrder,
  changeSortOrder,
  filterBy,
  setFilterBy,
}: SenderListControlsProps) => {
  return (
    <div id="sender-list-controls">
      <SortBar sortOrder={sortOrder} changeSortOrder={changeSortOrder} />
      <SearchInput
        name="sender-filter"
        label="Search senders:"
        value={filterBy}
        onChange={setFilterBy}
      />
    </div>
  );
};
