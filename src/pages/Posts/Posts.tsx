import React, { useEffect } from 'react';

import { useAuthenticationContext } from '../../auth/AuthenticationContext';
import { usePosts } from '../../posts/usePosts';
import { SendersList } from './SendersList';
import { PostsList } from './PostsLists';

import './posts.css';
import { UserDetails } from './UserDetails';

interface PostsPageProps {
  selectedSender?: string;
}

export const Posts = ({ selectedSender }: PostsPageProps) => {
  const [postsState, , selectSender] = usePosts();
  const { authenticationState, dispatch } = useAuthenticationContext();

  useEffect(() => {
    if (selectedSender) {
      selectSender(selectedSender);
    } else {
      selectSender(null);
    }
  }, [selectedSender, selectSender]);

  return (
    <div className="posts-layout">
      <div className="header">
        <h1>Posts</h1>
        <UserDetails
          name={authenticationState.user?.name || ''}
          logOut={() =>
            dispatch ? dispatch({ type: 'log_out', payload: {} }) : null
          }
        />
      </div>
      <div className="sidebar">
        <SendersList
          senders={postsState.senders}
          selectedSender={postsState.selectedSender}
          selectSender={selectSender}
        />
      </div>
      <PostsList
        selectedSender={postsState.selectedSender}
        posts={postsState.posts}
        loadingPosts={postsState.loading}
      />
    </div>
  );
};
