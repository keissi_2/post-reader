import React, { useState, useEffect, useMemo } from 'react';
import { Link } from 'react-router-dom';
import { flow } from 'lodash/fp';

import { Sender } from './Sender/Sender';
import { SenderListControls } from './SenderListControls';
import { sortArray, sortFnWithOrder } from '../../utils';

import type { SortOrder } from '../../utils';
import type { Sender as SenderType } from '../../posts/usePosts';

interface SendersProps {
  senders: {
    [key: string]: SenderType;
  };
  selectedSender: string | null;
  selectSender: (senderId: string | null) => void;
}

interface SenderWithId extends SenderType {
  id: string;
}

const mapSendersWithIds = (senders: SendersProps['senders']): SenderWithId[] =>
  Object.keys(senders).map((senderId) => ({
    id: senderId,
    ...senders[senderId],
  }));

const sortSendersByName = (sortOrder: SortOrder) =>
  sortFnWithOrder<SenderWithId>(sortOrder, (sender) => sender.name);

const filterSenders =
  (filterBy: string) =>
  (sender: SenderWithId): boolean => {
    if (filterBy.length < 3) {
      return true;
    }
    return sender.name.toLowerCase().indexOf(filterBy.toLowerCase()) !== -1;
  };

export const SendersList = ({
  senders,
  selectedSender,
  selectSender,
}: SendersProps) => {
  const [filterBy, setFilterBy] = useState('');
  const [sortOrder, setSortOrder] = useState<SortOrder>('Ascending');
  const [sortedSenders, setSortedSenders] = useState<SenderWithId[]>([]);

  const sortAndFilterSenders = useMemo(
    () =>
      flow(
        mapSendersWithIds,
        (senders: SenderWithId[]) => senders.filter(filterSenders(filterBy)),
        (senders) => sortArray(senders, sortSendersByName(sortOrder))
      ),
    [filterBy, sortOrder]
  );

  useEffect(() => {
    setSortedSenders(sortAndFilterSenders(senders));
  }, [senders, sortAndFilterSenders]);

  return (
    <div id="senders-list">
      <SenderListControls
        changeSortOrder={setSortOrder}
        sortOrder={sortOrder}
        filterBy={filterBy}
        setFilterBy={setFilterBy}
      />
      <div className="senders hide-scrollbar">
        {sortedSenders.map((sender) => (
          <Link
            className="sender-link"
            key={sender.id}
            to={(location) => {
              const [userId] = location.pathname.split('/').reverse();
              if (userId === sender.id) {
                return '/posts';
              }

              return `/posts/${sender.id}`;
            }}
          >
            <Sender
              key={sender.id}
              {...sender}
              isSelected={selectedSender === sender.id}
            />
          </Link>
        ))}
      </div>
    </div>
  );
};
