import React from 'react';

import './search-input.css';

interface SearchInputProps {
  name: string;
  className?: string;
  label: string;
  value: string;
  onChange: (value: string) => void;
}

export const SearchInput = ({
  name,
  className,
  value,
  label,
  onChange,
}: SearchInputProps) => {
  return (
    <div className={`search-input ${className}`}>
      <label htmlFor={name}>{label}</label>
      <input
        type="text"
        value={value}
        onChange={(e) => onChange(e.currentTarget.value)}
      />
    </div>
  );
};
