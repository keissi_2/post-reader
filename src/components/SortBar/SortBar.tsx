import React from 'react';
import { FiArrowUp, FiArrowDown } from 'react-icons/fi';

import type { SortOrder } from '../../utils';

import './sort-bar.css';

interface SortBarProps {
  sortOrder: SortOrder;
  changeSortOrder: (sortOrder: SortOrder) => void;
}

export const SortBar = ({ sortOrder, changeSortOrder }: SortBarProps) => {
  return (
    <div className="sort-bar">
      <button onClick={() => changeSortOrder('Ascending')}>
        <FiArrowUp size={24} strokeWidth={sortOrder === 'Ascending' ? 4 : 2} />
      </button>
      <button onClick={() => changeSortOrder('Descending')}>
        <FiArrowDown
          size={24}
          strokeWidth={sortOrder === 'Descending' ? 4 : 2}
        />
      </button>
    </div>
  );
};
