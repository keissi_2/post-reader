import React from 'react';

import './loading-indicator.css';

export const LoadingIndicator = () => {
  return (
    <div className="loading-indicator">
      <span className="dot first" />
      <span className="dot second" />
      <span className="dot third" />
    </div>
  );
};
