import { makeRequest } from '../utils';

export interface PostsResponse {
  meta: {
    request_id: string;
  };
  data: {
    page: number;
    posts: Array<{
      id: string;
      from_name: string;
      from_id: string;
      message: string;
      type: string;
      created_time: string;
    }>;
  };
}

export const fetchPosts = async (token: string, pageNumber: number) => {
  const [data, error] = await makeRequest<PostsResponse>(
    'GET',
    'https://api.supermetrics.com/assignment/posts',
    [`sl_token=${token}`, `page=${pageNumber}`]
  );

  if (data) {
    return data.data;
  }

  return error;
};
