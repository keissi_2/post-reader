import { useEffect, useReducer, useMemo } from 'react';
import { useAuthenticationContext } from '../auth/AuthenticationContext';
import { fetchPosts, PostsResponse } from './fetchPosts';

export interface PostsMap {
  loading: boolean;
  error: any;
  posts: PostsResponse['data']['posts'];
  senders: {
    [key: string]: Sender;
  };
  selectedSender: string | null;
}

export interface Sender {
  name: string;
  postCount: number;
}

const initialState = {
  loading: false,
  error: null,
  posts: [],
  senders: {},
  selectedSender: null,
};

const postsReducer = (
  state: PostsMap,
  action: { type: string; payload?: { [key: string]: any } }
) => {
  switch (action.type) {
    case 'add_posts': {
      const senders = mapUniqueSenders(action.payload?.posts, state.senders);

      return {
        ...state,
        posts: [...state.posts, ...action.payload?.posts],
        senders: { ...senders },
      };
    }
    case 'set_selected_sender': {
      return { ...state, selectedSender: action.payload?.sender };
    }
    case 'loading_posts': {
      return { ...state, loading: true };
    }
    case 'clear_loading': {
      return { ...state, loading: false };
    }
    case 'error': {
      return { ...state, loading: false, error: action.payload?.error };
    }
    default:
      return state;
  }
};

const mapUniqueSenders = (
  posts: PostsResponse['data']['posts'],
  senders: { [key: string]: Sender } = {}
) => {
  return posts.reduce((senders: { [key: string]: Sender }, post) => {
    const sender = post.from_id;

    if (senders[sender]) {
      return {
        ...senders,
        [sender]: {
          ...senders[sender],
          postCount: senders[sender].postCount + 1,
        },
      };
    }

    return { ...senders, [sender]: { name: post.from_name, postCount: 1 } };
  }, senders);
};

const loadPostsForPage =
  (
    token: string,
    dispatch: React.Dispatch<{
      type: string;
      payload?: { [key: string]: any };
    }>
  ) =>
  async (pageNumber: number, fetchAll = false): Promise<void> => {
    dispatch({ type: 'loading_posts' });
    const posts = await fetchPosts(token, pageNumber);
    if (posts instanceof Error || posts === null) {
      dispatch({ type: 'error', payload: { error: posts } });
      return;
    }

    if (pageNumber > posts.page) {
      return dispatch({ type: 'clear_loading' });
    }

    dispatch({
      type: 'add_posts',
      payload: { pageNumber: posts.page, posts: posts.posts },
    });

    if (fetchAll) {
      loadPostsForPage(token, dispatch)(posts.page + 1, fetchAll);
    }
  };

export const usePosts = (): [
  PostsMap,
  (pageNumber: number, fetchAll: boolean) => void,
  (senderId: string | null) => void
] => {
  const authContext = useAuthenticationContext();
  if (!authContext || !authContext.authenticationState.token?.value) {
    throw new Error('User not authenticated!');
  }

  const { value: tokenValue } = authContext.authenticationState.token;
  const [posts, dispatch] = useReducer(postsReducer, initialState);
  const selectSender = useMemo(
    () => (senderId: string | null) =>
      dispatch({ type: 'set_selected_sender', payload: { sender: senderId } }),
    []
  );

  const loadPosts = useMemo(
    () => loadPostsForPage(tokenValue, dispatch),
    [dispatch, tokenValue]
  );

  useEffect(() => {
    loadPostsForPage(tokenValue, dispatch)(1, true);
  }, [tokenValue]);

  return [posts, loadPosts, selectSender];
};
