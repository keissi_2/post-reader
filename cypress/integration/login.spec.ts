/// <reference types="cypress" />

describe('Login and logout, assert that posts are fetched', () => {
  it('Not logged in user should be redirected to /login', () => {
    cy.visit('/').then(() =>
      cy.location().should((location) => {
        expect(location.pathname).to.eq('/login');
      })
    );
  });

  it('User can login', () => {
    cy.get('input[name="name"]').type('Testman');
    cy.get('input[name="email"]').type('email@mail.fi');

    cy.intercept('GET', '**/assignment/posts*').as('getPosts');

    cy.get('#login-form')
      .submit()
      .then(() => {
        cy.location()
          .should((location) => {
            expect(location.pathname).to.eq('/posts');
          })
          .then(() => {
            cy.wait('@getPosts');
          });
      });
  });

  it('User can logout', () => {
    cy.get('.logout-button')
      .click()
      .then(() =>
        cy.location().should((location) => {
          expect(location.pathname).to.eq('/login');
        })
      );
  });
});
